# **LIS4905 Directed Independent Study**

## **Team members - Fall 17:**

    * [Benjamin Bloodworth:](bbloodworth/README.md "Ben's repo") bbloodworth@fsu.edu
	* [Ariana Davis:](amd14b/README.md "Ariana's repo") amd14b@my.fsu.edu
	* [Kyle Kane:](ktk12/README.md "Kyle's repo") ktk12@my.fsu.edu	
	* [Jee Kim:](jhk13c/README.md "Jee's repo") jhk13c@my.fsu.edu
	* [Stephen Keating:](smk16f/README.md "Stephen's repo") smk16f@my.fsu.edu
	* [Jonathan Sapp:](jss14e/README.md "Jonathan's repo") jss14e@my.fsu.edu
	* [Landon Thaler:](lmt14b/README.md "Landon's repo") lmt14b@my.fsu.edu
	* [Andrew Whitely:](anw14b/README.md "Andrew's repo") anw14b@my.fsu.edu

**Links match subdirectory paths above:**

- [Benjamin Bloodworth:](bbloodworth/README.md "Ben's repo") bbloodworth@fsu.edu
- [Ariana Davis:](amd14b/README.md "Ariana's repo") amd14b@my.fsu.edu
- [Kyle Kane:](ktk12/README.md "Kyle's repo") ktk12@my.fsu.edu	
- [Jee Kim:](jhk13c/README.md "Jee's repo") jhk13c@my.fsu.edu
- [Stephen Keating:](smk16f/README.md "Stephen's repo") smk16f@my.fsu.edu
- [Jonathan Sapp:](jss14e/README.md "Jonathan's repo") jss14e@my.fsu.edu
- [Landon Thaler:](lmt14b/README.md "Landon's repo") lmt14b@my.fsu.edu
- [Andrew Whitely:](anw14b/README.md "Andrew's repo") anw14b@my.fsu.edu


## **Test Development Environment:**

[![PowerLine:](https://static.wixstatic.com/media/712a4d_7594db9d5a2e4e2a999a51d096e70aa7~mv2.png/v1/fill/w_431,h_228,al_c,usm_0.66_1.00_0.01/712a4d_7594db9d5a2e4e2a999a51d096e70aa7~mv2.png)](https://codesolve-eval.azurewebsites.net "PowerLine CodeSolve Portal")


## **Course Texbook:**
**Murach's PHP/MySQL 2nd Ed.**

[![Murach's PHP and MySQL - 2nd Ed.](https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRPIYm6JISdonZdLCDDr_WC0W0h3nf9aCvq2TZltFBALecRXMkE)](http://murach.com/books/php2/index.htm "Murach's PHP and MySQL - 2nd Ed.")


## **Resource Links:**
- [PowerLine_Getting_Started_V01.pdf](docs/PowerLine_Getting_Started_V01.pdf)
- [Useful Links](http://www.qcitr.com/usefullinks.htm)
- [DB Links](http://www.qcitr.com/dblinks.htm)
- [Collaborate Participant Link](https://sas.elluminate.com/m.jnlp?password=M.D0948187AD12C6A5963CEDDD1AA9C1&sid=9452)
- [Collaborate Recordings Link](https://sas.elluminate.com/mrtbl?suid=M.EC1496FBD8DF5C19C21E735FEFCE25&sid=9452)


## **Links and Screenshots (demo link examples)**:

### Links:

**Generic Syntax:**

```
[Alt text](path/to/img.jpg "Optional title")
```

**Example:** (use no exclamation point)

```
[Pet Store ERD](img/Pet_Store.png "Pet Store ERD")
```

[Pet Store ERD](img/Pet_Store.png "Pet Store ERD")

### Embedded Images:

**Generic Syntax:**

```
![Alt text](path/to/img.jpg "Optional title")
```

**Example:** (embedded images require an exclamation mark/point)

```
![Pet Store ERD](img/Pet_Store.png "Pet Store ERD")
```

## Pet Store ERD

![Pet Store ERD](img/Pet_Store.png "Pet Store ERD")


**Horizontal Rule**: 
`---`

---


===================================

## **Preliminary Requirements:**
## 1. **Git Tutorials: (works for both Github and Bitbucket):**

**First: Complete the Following Two Tutorials:**

[Git Tutorial](https://confluence.atlassian.com/bitbucket/git-tutorial-keep-track-of-your-space-station-locations-759857287.html "Working with Git")

[Tutorial: Request to update a teammate's repository](https://confluence.atlassian.com/bitbucket/tutorial-request-to-update-a-teammate-s-repository-774243385.html "Working within a Team Environment")

    a. Set up Git
	
	b. Create a Bitbucket account and a Git repository
	
	c. Clone your Git repository and add source files
	
	d. Create a file and pull changes from Bitbucket
	
	e. Use Git branches
	
	f. Create a Bitbucket Team
	
	
 * [git - the simple guide](http://rogerdudler.github.io/git-guide/)
 * [Very good overview](http://blogs.atlassian.com/2014/10/git-workflows-saas-teams-webinar-recording/)
 * [Tutorials](https://www.atlassian.com/git/)
 * [Git: Basic Tricks](http://www-cs-students.stanford.edu/~blynn/gitmagic/ch02.html)
 * [Git Basics: Getting a repository](http://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)


## **Git - Team Workflow**

**Github Tutorial For Beginners:**

[![Github Tutorial For Beginners:](https://i.ytimg.com/an_webp/0fKg7e37bQE/mqdefault_6s.webp?du=3000&sqp=COjb484F&rs=AOn4CLCfwxCcpcAFP3Qn80OmLUjj_z4o7Q)](https://www.youtube.com/watch?v=0fKg7e37bQE "Github Tutorial For Beginners")

**Branching, Merging & Team Workflow:**

[![Branching, Merging & Team Workflow:](https://i.ytimg.com/an_webp/oFYyTZwMyAg/mqdefault_6s.webp?du=3000&sqp=CKjm484F&rs=AOn4CLCuSuQH1hl6GXiL3ipn6ZWdWV67EA)](https://www.youtube.com/watch?v=oFYyTZwMyAg&t=47s "GITHUB PULL REQUEST, Branching, Merging & Team Workflow")

- [Resolving Merge Conflicts: ](https://help.github.com/articles/resolving-a-merge-conflict-using-the-command-line/ "Resolving Merge Conflicts")
- [Adopt a Git Branching Strategy: ](https://docs.microsoft.com/en-us/vsts/git/concepts/git-branching-guidance "Adopt a Git Branching Strategy")
- [Pro Git Book V2: ](https://git-scm.com/book/en/v2 "Pro Git Book V2")


## **Markdown Syntax Resources**:	
- [x] [http://daringfireball.net/projects/markdown/basics](http://daringfireball.net/projects/markdown/basics)
- [x] [https://confluence.atlassian.com/display/STASH/Markdown+syntax+guide](https://confluence.atlassian.com/display/STASH/Markdown+syntax+guide)
- [x] [https://bitbucket.org/tutorials/markdowndemo](https://bitbucket.org/tutorials/markdowndemo)
- [x] [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)



## 2. **Twitter Bootstrap:**

 * [Download Twitter Bootstrap](http://getbootstrap.com/)
 * Use the first download link (also displays documentation): "Bootstrap: Compiled and minified CSS, JavaScript, and fonts. No docs or original source files are included."
 * Unzip the folder into your "test" directory, and rename it "bootstrap."
 * You don't need to download anything else.
 * Copy and paste the "Basic template" code and save it as index.htm, in the "bootstrap" directory.
 * jQuery is required. However, you should already have downloaded it. Place the min.js version that you downloaded into bootstrap's js directory. (Use local, relative paths to helper files; otherwise, an Internet connection wlll be needed to test development.)
 * Test it!
 * [Review Twitter Bootstrap Examples](http://getbootstrap.com/getting-started/#examples)



**Review the following tutorials:**

 * [https://www.youtube.com/watch?v=E_BrfH10OTc](https://www.youtube.com/watch?v=E_BrfH10OTc)

 * [http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/](http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/)

 * [http://www.tutorialspoint.com/bootstrap](http://www.tutorialspoint.com/bootstrap)

 * [http://www.w3schools.com/bootstrap/](http://www.w3schools.com/bootstrap/)


**Bootstrap Grid System:**

 * [http://www.w3schools.com/bootstrap/bootstrap_grid_system.asp](http://www.w3schools.com/bootstrap/bootstrap_grid_system.asp)

 * [http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-tables.php](http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-tables.php)


**Note:** Realize that if you get the Bootstrap CDN versions, you won't
  be able to develop w/o an Internet connection. However, the CDN
  versions *should* be used in production environments. 


**Interactive Tutorials:**

The following Web sites and tools can better help you to communicate
code with other people on the Web--they can help you, or you can help
others:

* [Try Git](https://try.github.io/levels/1/challenges/1)
* [jsfiddle.net](http://jsfiddle.net/)
* [pastebin.com](http://pastebin.com/)
* [codepen.io](http://codepen.io/)


## 3. **Additional Resources/References:**

### Python
[The Python Tutorial](https://docs.python.org/3.4/tutorial/index.html)

[Python for Non-Programmers](https://wiki.python.org/moin/BeginnersGuide/NonProgrammers)

[Python for Programmers](https://wiki.python.org/moin/BeginnersGuide/Programmers)

1. [Python Download](https://www.python.org/downloads/)

2. [Python Installation](http://www.howtogeek.com/197947/how-to-install-python-on-windows/)

### Django
1. [Writing your first Django app, part 1](https://docs.djangoproject.com/en/1.8/intro/tutorial01/)
 
2. [Writing your first Django app, part 2](https://docs.djangoproject.com/en/1.8/intro/tutorial02/) 
	  
### MongoDB
[Getting Started with MongoDB](https://docs.mongodb.org/getting-started/shell/)

[The MongoDB Tutorial ](http://www.hacksparrow.com/the-mongodb-tutorial.html)


## **MEAN Stack Tutorial** (includes Yeoman and testing libraries):
1. [http://www.ibm.com/developerworks/library/wa-mean1/index.html](http://www.ibm.com/developerworks/library/wa-mean1/index.html)
2. [http://www.ibm.com/developerworks/library/wa-mean2/index.html](http://www.ibm.com/developerworks/library/wa-mean2/index.html)
3. [http://www.ibm.com/developerworks/library/wa-mean3/index.html](http://www.ibm.com/developerworks/library/wa-mean3/index.html)
4. [http://www.ibm.com/developerworks/library/wa-mean4/index.html](http://www.ibm.com/developerworks/library/wa-mean4/index.html)
5. [http://www.ibm.com/developerworks/web/library/wa-mean5/index.html](http://www.ibm.com/developerworks/web/library/wa-mean5/index.html)


#### More Markdown syntax:


## Live demo:

http://comingsoon.com/examples/


You also can run the ```demo```:


* Clone repo (when content becomes available): 

```
git clone https://mjowett@bitbucket.org/mjowett/lis4905fall17.git
```

* Access demo:

```
http://localhost/path_to_repos/lis4905fall17
```


### Markdown Styles:

*Italics*:	

`*italics*`

or...

`_italics_`


**Bold**:	

`**bold**`

or...

`__bold__`

***Bold Italics***:	

`***bold italics***`

or...

`___bold italics___`


~~Strikethrough~~:	 

`~~Strikethrough~~`


### Adding Code:
Include various code snippets in README.md. Then wrap them in a `code block`.


#### Syntax Highlighting:
[Code Syntax Highlighting](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code "Highlight Code Sytax")


#### Code Blocks:
`` `one line code block` ``

`one line code block`

#### HTML:
```html
<form>
	<input required>
</form>
<script src="jquery.js"></script>
<script src="jquery.validate.js"></script>
<script>
$("form").validate();
</script>
```

#### JavaScript:
```js
var x = 5;
var y = 6;
var z = x + y;
document.getElementById("demo").innerHTML = z;
```

### Adding Youtube videos:

```
[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](http://www.youtube.com/watch?v=YOUTUBE_VIDEO_ID_HERE "link title")
```

**Example:**
[![10 Muscle Cars of All Time](https://i.ytimg.com/an_webp/n4nQKRIteGE/mqdefault_6s.webp?du=3000&sqp=CJeR5M4F&rs=AOn4CLDMGukeGd_e0H_kACRhvOZflM3GGg)](https://www.youtube.com/watch?v=n4nQKRIteGE "Top 10 Muscle Cars")


**Another:**
[![The World's Fastest Pistol](https://i.ytimg.com/an_webp/mymewEQiQhY/mqdefault_6s.webp?du=3000&sqp=CKbA5M4F&rs=AOn4CLDT9XkQHUNR260x_KYglDvksHgmxg)](https://www.youtube.com/watch?v=mymewEQiQhY&feature=youtu.be "FK BRNO")


## Features

See the [Form Validation](http://formvalidation.io) for the full list of features

## Download

* Latest version: [v0.0.1](https://mjowett@bitbucket.org/mjowett/lis4905fall17), released on 2017-28-17
* Older versions: [Releases](https://mjowett@bitbucket.org/mjowett/lis4905fall17/releases) page
* Release History: See [Change Log](CHANGELOG.md)

## Documentation

* [Official website](https://mjowett@bitbucket.org/mjowett/lis4905fall17)


## Contribution

Contributions are welcome **and** mandatory!  :)

## License

For more information about the license, see http://example.com/license/

Copyright (c) 2017 **lis4905fall17**

Licensed under the **Git 'R Dun** license.
