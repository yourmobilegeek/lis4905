# **LIS4905 Directed Independent Study**

## Kyle Kane

### Team mate - Me, myself, and I

### Finished tasks: 
	* Branched Bitbucket
	* Added README.md
	* Petstore web application in Powerline

### Issues:
	* I used the online GUI and some minor bugs e.g. controls reseting, attributes changing data types.
	* Also had some connectivity issues with the online tool.
	* Broad issue of mine is that it feels like relearning a basic web application course with different names.