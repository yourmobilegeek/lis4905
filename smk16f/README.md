# LIS4905 Directed Independent Study

## Stephen Keating

### Team mate:
- [Jee Kim:](../jhk13c/README.md "Jee's repo") jhk13c@my.fsu.edu

#### Finished Tasks
* Created this README.md file.
* Created powerline app using excel spread sheet.

#### Issues

* Using the GUI to create Forms, the table with all the fields runs off the side of the screen.
* I could not set foriegn key values in the user interface, but could upload them from a spreadsheet.
* In the DB Dropdown section of the application, DDL property 1 did not have any values unless I imported a spreadsheet.
* I cannot figure out how to upload data to the application.


*Screenshot of issue with forms fields*:
![Issue with forms fields running over the edge of the screen.](img/forms_field_issue1.PNG)
